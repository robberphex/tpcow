module github.com/esec/tpcow

go 1.15

require (
	github.com/aead/chacha20 v0.0.0-20180709150244-8b13a72661da // indirect
	github.com/cyfdecyf/bufio v0.0.0-20130801052708-9601756e2a6b
	github.com/cyfdecyf/color v0.0.0-20130827105946-31d518c963d2
	github.com/cyfdecyf/leakybuf v0.0.0-20140618011800-ffae040843be
	github.com/shadowsocks/shadowsocks-go v0.0.0-20190614083952-6a03846ca9c0
	golang.org/x/crypto v0.0.0-20190829043050-9756ffdc2472 // indirect
)
